//SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.13;

import "./RentableItem.sol";
import "./Whitelist.sol";
import "../node_modules/@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "../node_modules/@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../node_modules/@openzeppelin/contracts/token/ERC721/IERC721Receiver.sol";
import "node_modules/@openzeppelin/contracts/utils/structs/EnumerableSet.sol";

contract ItemManager is IERC721Receiver {
    using EnumerableSet for EnumerableSet.UintSet;
    using EnumerableSet for EnumerableSet.AddressSet;

    RentableItem public immutable itemNFT;
    IERC20 public immutable token;
    address public immutable owner;

    uint256 constant HOUR = 60 * 60;
    uint256 constant DAY = 60 * 60 * 24;

    struct Rental {
        uint256 startTimestamp;
        uint256 duration;
        address holder; 
    }

    mapping(uint256 => Rental) private itemRentals;
    mapping(address => EnumerableSet.UintSet) private userItems;

    EnumerableSet.AddressSet private blackList;

    constructor(address token_, address itemNFT_) {
        token = IERC20(token_);
        itemNFT = RentableItem(itemNFT_);
        require(msg.sender == itemNFT.list().admin(), "ADMIN CHANGED");
        owner = msg.sender;
    }

    function startRent(uint256 id, uint256 duration) external {
        require(itemNFT.list().inWhiteList(msg.sender), "FORBIDDEN");
        require(itemNFT.ownerOf(id) == address(this), "ALREADY RENT");

        uint256 rentalUnit = itemNFT.getItemInfo(id).t == RentableItem.ItemType.CAR ? HOUR : DAY;
        require(duration >= rentalUnit, "TOO SHORT RENTAL DURATION");
        uint256 unitsInDuration = (duration % rentalUnit == 0) ? (duration / rentalUnit) : (duration / rentalUnit + 1);
        token.transferFrom(msg.sender, address(this), itemNFT.getItemInfo(id).pricePerRentalUnit * unitsInDuration);
        token.transfer(itemNFT.getItemInfo(id).owner, itemNFT.getItemInfo(id).pricePerRentalUnit * unitsInDuration);
        itemNFT.safeTransferFrom(address(this), msg.sender, id);

        Rental memory rental_ = Rental(block.timestamp, duration, msg.sender);
        itemRentals[id] = rental_;
        userItems[msg.sender].add(id);
    }

    function claimItem(uint256 id, bool claimExtra) external {
        require(itemNFT.list().inWhiteList(msg.sender), "FORBIDDEN");
        require(itemNFT.ownerOf(id) != address(this), "NOT RENT");
        RentableItem.ItemInfo memory item = itemNFT.getItemInfo(id);
        require(item.owner == msg.sender, "NOT OWNER");
        uint256 end = itemRentals[id].startTimestamp + itemRentals[id].duration;
        require(block.timestamp >= end, "RENTAL NOT FINISHED YET");

        if (claimExtra) {
            uint256 rentalUnit = item.t == RentableItem.ItemType.CAR ? HOUR : DAY;
            uint256 extraTime = block.timestamp - end;
            uint256 unitsInDuration = (extraTime % rentalUnit == 0) ? (extraTime / rentalUnit) : (extraTime / rentalUnit + 1);
            token.transferFrom(itemRentals[id].holder, address(this), item.pricePerRentalUnit * unitsInDuration);
            token.transfer(msg.sender, item.pricePerRentalUnit * unitsInDuration);
        }
        
        itemNFT.safeTransferFrom(itemNFT.ownerOf(id), address(this), id);
        userItems[itemRentals[id].holder].remove(id);
    }

    function addItem(RentableItem.ItemInfo memory item_) external {
        require(msg.sender == owner, "FORBIDDEN");
        itemNFT.mint(item_);
    }

    function removeItem(uint256 id) external {
        require(msg.sender == owner, "FORBIDDEN");
        itemNFT.burn(id);
    }

    function myRentItems() external view returns (uint256[] memory) {
        return userItems[msg.sender].values();
    }

    function banUser(address user) external {
        require(msg.sender == owner, "NOT OWNER");
        blackList.add(user);
    }

    function onERC721Received(
        address operator,
        address from,
        uint256 tokenId,
        bytes calldata data
    ) external returns (bytes4) {
        return this.onERC721Received.selector;
    }
}
