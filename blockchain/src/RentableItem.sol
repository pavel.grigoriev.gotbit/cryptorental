// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import "../node_modules/@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "./Whitelist.sol";

contract RentableItem is ERC721 {
    uint256 public topId = 1;

    Whitelist public list;
    address manager;

    enum ItemType {
        CAR,
        FLAT
    }

    struct ItemInfo {
        address owner;
        uint256 pricePerRentalUnit;
        ItemType t;
        string name;
        string description;
    }

    mapping(uint256 => ItemInfo) public itemById;

    constructor(address list_) ERC721("CryptoRental", "CRN") {
        list = Whitelist(list_);
    }

    function setItemManager(address manager_) external {
        require(msg.sender == list.admin() || msg.sender == list.delegatedAdmin(), "FORBIDDEN");
        manager = manager_;
    }

    function mint(ItemInfo memory item_) external {
        require(msg.sender == manager, "FORBIDDEN");
        _mint(manager, topId);
        itemById[topId] = item_;
        ++topId;
    }

    function burn(uint256 id) external {
        require(ownerOf(id) == manager, "ITEM IS IN USE");
        _burn(id);
        delete itemById[id];
    }

    function getItemInfo(uint256 id) external view returns (ItemInfo memory) {
        ItemInfo memory info = itemById[id];
        return info;
    }
}
