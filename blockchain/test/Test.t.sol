// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import "forge-std/Test.sol";
import "../src/Whitelist.sol";
import "../src/RentableItem.sol";
import "../src/ItemManager.sol";
import "../src/Token.sol";
import "./RandomAddress.sol";
import {console} from "forge-std/console.sol";

contract WhitelistTest is RandomAddress, Test {
    function testCreation() public {
        address owner = randomAddress();
        address user = randomAddress();

        vm.startPrank(owner);
        Whitelist list = new Whitelist();
        list.addToWhiteList(user, "", "", "", "", "");
        RentableItem nft = new RentableItem(address(list));
        Token token = new Token();
        ItemManager manager = new ItemManager(address(token), address(nft));
        nft.setItemManager(address(manager));
        RentableItem.ItemInfo memory item_1 = RentableItem.ItemInfo(owner, 10, RentableItem.ItemType.CAR, "car", "desc");
        RentableItem.ItemInfo memory item_2 = RentableItem.ItemInfo(user, 100000, RentableItem.ItemType.FLAT, "flat", "description");
        manager.addItem(item_1);
        manager.addItem(item_2);

        token.increaseAllowance(address(manager), 100000 * 10);
        uint256 duration = 60 * 60 * 24 * 10;
        manager.startRent(2, duration);
        token.increaseAllowance(address(manager), 100000);
        nft.approve(address(manager), 2);
        vm.stopPrank();

        vm.warp(block.timestamp + duration + duration / 2);

        vm.startPrank(user);

        manager.claimItem(2, false);

        vm.stopPrank();
    }
}