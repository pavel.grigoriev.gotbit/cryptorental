// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import "forge-std/Script.sol";
import "../src/ItemManager.sol";
import "../src/RentableItem.sol";
import "../src/Whitelist.sol";
import "../src/Token.sol";

contract DeployScript is Script {
    function setUp() public {}

    function run() public {
        vm.startBroadcast();

        Whitelist list = new Whitelist();
        RentableItem nft = new RentableItem(address(list));
        Token token = new Token();
        ItemManager manager = new ItemManager(address(token), address(nft));
        nft.setItemManager(address(manager));
        console.log("Whitelist Contract: ", address(list));
        console.log("RentableItem Contract: ", address(nft));
        console.log("Token Contract: ", address(token));
        console.log("ItemManager Contract: ", address(manager));
        
        vm.stopBroadcast();
    }
}
