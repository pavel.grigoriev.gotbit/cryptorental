(function() {
   
    'use strict';
   
    $('.input-file').each(function() {
      var $input = $(this),
          $label = $input.next('.js-labelFile'),
          labelVal = $label.html();
       
     $input.on('change', function(element) {
        var fileName = '';
        if (element.target.value) fileName = element.target.value.split('\\').pop();
        fileName ? $label.addClass('has-file').find('.js-fileName').html(fileName) : $label.removeClass('has-file').html(labelVal);
     });
    });
   
  })();
  (function() {
   
    'use strict';
   
    $('.input-file2').each(function() {
      var $input = $(this),
          $label = $input.next('.js-labelFile2'),
          labelVal = $label.html();
       
     $input.on('change', function(element) {
        var fileName = '';
        if (element.target.value) fileName = element.target.value.split('\\').pop();
        fileName ? $label.addClass('has-file').find('.js-fileName2').html(fileName) : $label.removeClass('has-file').html(labelVal);
     });
    });
   
  })();
  (function() {
   

   
    $('.input-file3').each(function() {
      var $input = $(this),
          $label = $input.next('.js-labelFile3'),
          labelVal = $label.html();
       
     $input.on('change', function(element) {
        var fileName = '';
        if (element.target.value) fileName = element.target.value.split('\\').pop();
        fileName ? $label.addClass('has-file').find('.js-fileName3').html(fileName) : $label.removeClass('has-file').html(labelVal);
     });
    });
   
  })();